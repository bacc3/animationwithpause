//
//  ViewController.m
//  AnimationWithPause
//
//  Created by Vasiliy Korchagin on 17/01/2017.
//  Copyright © 2017 Vasily Korchagin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) UIView *greenSquareView;
@property (nonatomic, strong) UIButton *pauseButton;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createAndLayoutSquareView];
    [self createAndLayoutButtons];
    [self startNSLogTimer];
}

- (void)createAndLayoutSquareView {
    self.greenSquareView = [[UIView alloc] initWithFrame:CGRectMake(0, 150, 50, 50)];
    self.greenSquareView.backgroundColor = [UIColor greenColor];
    [self.view addSubview:self.greenSquareView];
}

- (void)createAndLayoutButtons {
    self.pauseButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)/2-50, 210, 100, 50)];
    [self.pauseButton setTitle:@"pause" forState:UIControlStateNormal];
    self.pauseButton.backgroundColor = [UIColor greenColor];
    [self.pauseButton addTarget:self action:@selector(pauseAnimation) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.pauseButton];
    
    self.playButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)/2-50, 210, 100, 50)];
    [self.playButton setTitle:@"play" forState:UIControlStateNormal];
    self.playButton.backgroundColor = [UIColor greenColor];
    [self.playButton addTarget:self action:@selector(resumeAnimation) forControlEvents:UIControlEventTouchUpInside];
    self.playButton.hidden = YES;
    self.playButton.enabled = NO;
    [self.view addSubview:self.playButton];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self addAnimationToGreenSquareView];
}

- (void)addAnimationToGreenSquareView {
    CABasicAnimation *noteAnimation = [CABasicAnimation animation];
    noteAnimation.keyPath = @"position.x";
    noteAnimation.byValue = [NSNumber numberWithFloat:CGRectGetWidth(self.view.frame) - 50];
    noteAnimation.duration = 1.f;
    noteAnimation.fillMode = kCAFillModeBoth;
    noteAnimation.removedOnCompletion = NO;
    noteAnimation.autoreverses = YES;
    noteAnimation.repeatCount = HUGE;
    noteAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [self.greenSquareView.layer addAnimation:noteAnimation forKey:@"noteAnimation"];
}

- (void)pauseAnimation {
    CFTimeInterval pausedTime = [self.greenSquareView.layer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.greenSquareView.layer.speed = 0.0;
    self.greenSquareView.layer.timeOffset = pausedTime;
    self.pauseButton.hidden = YES;
    self.pauseButton.enabled = NO;
    self.playButton.hidden = NO;
    self.playButton.enabled = YES;
    NSLog(@"pause animation");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.005 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.timer.isValid) {
            [self.timer invalidate];
        }
    });
}

- (void)resumeAnimation {
    CFTimeInterval pausedTime = [self.greenSquareView.layer timeOffset];
    self.greenSquareView.layer.speed = 1.0;
    self.greenSquareView.layer.timeOffset = 0.0;
    self.greenSquareView.layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.greenSquareView.layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.greenSquareView.layer.beginTime = timeSincePause;
    self.pauseButton.hidden = NO;
    self.pauseButton.enabled = YES;
    self.playButton.hidden = YES;
    self.playButton.enabled = NO;
    NSLog(@"resume animation");
    [self startNSLogTimer];
}

- (void)startNSLogTimer {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.001f
                                                  target:self
                                                selector:@selector(NSLogGreenViewPosition)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)NSLogGreenViewPosition {
    NSLog(@"greenSquareView presentation layer X = %f", self.greenSquareView.layer.presentationLayer.frame.origin.x);
}

@end
