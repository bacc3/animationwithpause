//
//  main.m
//  AnimationWithPause
//
//  Created by Vasiliy Korchagin on 17/01/2017.
//  Copyright © 2017 Vasily Korchagin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
